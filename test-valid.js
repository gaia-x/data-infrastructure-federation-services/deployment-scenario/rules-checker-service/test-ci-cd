
let checks = 
[
    {   
        name:"testSchemaValidatorOk" , 
        url :  'https://schemavalidator.abc-federation.gaia-x.community/provider' ,  
        testfile  :'./datas/schema-validator/test-ok.json',
        testResult : function (body) {  return JSON.parse(body).statut },
        type: "post"
    }, 
    {   
        name:"testSchemaValidatorKo" , 
        url :  'https://schemavalidator.abc-federation.gaia-x.community/provider' ,  
        testfile : './datas/schema-validator/test-ko.json',
        testResult : function (body) {  return !JSON.parse(body).statut },
        type: "post"
    },
    {   
        name:"testParticipantRulesOk" , 
        url :  'https://ruleschecker.abc-federation.gaia-x.community/rules/compliance/complianceParticipant' ,  
        testfile : './datas/rules-checker/provider-test-ok.json',
        testResult : function (body) {  return body.statut=="true" },
        type: "post"
    },
    {   
        name:"testParticipantRulesKo" , 
        url :  'https://ruleschecker.abc-federation.gaia-x.community/rules/compliance/complianceParticipant' ,  
        testfile : './datas/rules-checker/provider-test-ko.json',
        testResult : function (body) {  return body.statut=="false" },
        type: "post"
    },
    {   
        name:"testServiceOfferingRulesOk" , 
        url :  'https://ruleschecker.abc-federation.gaia-x.community/rules/compliance/complianceServiceOffering' ,  
        testfile : './datas/rules-checker/serviceOffering-ok.json',
        testResult : function (body) {  return body.statut=="true" },
        type: "post"
    },
    {   
        name:"testServiceOfferingRulesOk" , 
        url :  'https://ruleschecker.abc-federation.gaia-x.community/rules/compliance/complianceServiceOffering' ,  
        testfile : './datas/rules-checker/serviceOffering-ok.json',
        testResult : function (body) {  return body.statut=="true" },
        type: "post"
    },
    {   
        name:"testServiceOfferingRulesKo" , 
        url :  'https://ruleschecker.abc-federation.gaia-x.community/rules/compliance/complianceServiceOffering' ,  
        testfile : './datas/rules-checker/serviceOffering-ko.json',
        testResult : function (body) {  return body.statut=="false" },
        type: "post"
    },
    {   
        name:"testLabellingRulesOk" , 
        url :  'https://ruleschecker.abc-federation.gaia-x.community/rules/labelling/label' ,
        testfile : './datas/rules-checker/labelling-ok.json',
        testResult : function (body) {  return body.statut=="true" },
        type: "post"
    },
    { 
        name:"testLabellingRulesKo" , 
        url :  'https://ruleschecker.abc-federation.gaia-x.community/rules/labelling/label' ,
        testfile : './datas/rules-checker/labelling-ko.json',
        testResult : function (body) {  return body.statut=="false" },
        type: "post"
    },
]

export default checks;