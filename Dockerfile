FROM node:alpine

WORKDIR /usr/src/app

COPY . .
RUN npm install

ENV testfile=./test-integration.js
CMD ["node", "index.js"]
