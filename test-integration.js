let checks = 
[
    {   
        name:"testSchemaValidatorOk" , 
        url :  'https://schemavalidator.abc-federation.gaia-x.community/provider' ,  
        testfile  :'./datas/schema-validator/test-ok.json',
        testResult : function (body) {  return JSON.parse(body).statut },
        type: "post"
    }, 
    { 
        name:"testParticipantRulesOk" , 
        url :  'https://ruleschecker.abc-federation.gaia-x.community/rules/compliance/complianceParticipant' ,  
        testfile : './datas/rules-checker/provider-test-ok.json',
        testResult : function (body) {  return body.statut=="true" },
        type: "post"
    },
    {
        name:"testChineseParticipant" , 
        url :  'https://ruleschecker.abc-federation.gaia-x.community/rules/compliance/complianceParticipant' ,  
        testfile : './datas/rules-checker/provider-chinese.json',
        testResult : function (body) {  return body.statut=="true" },
        type: "post"
    },
    {
        name:"testParticipantRulesOk" , 
        url :  'https://ruleschecker.abc-federation.gaia-x.community/rules/compliance/complianceParticipant' ,  
        testfile : './datas/rules-checker/provider-test-ok.json',
        testResult : function (body) {  return body.statut=="true" },
        type: "post"
    },
    {
        name:"testParticipantRulesOk_vcatalog" , 
        url :  'https://ruleschecker.abc-federation.gaia-x.community/rules/compliance/complianceParticipant' ,  
        testfile : './datas/rules-checker/s-provider.json',
        testResult : function (body) {  return body.statut=="true" },
        type: "post"
    },
    {
        name:"testParticipantRulesGeneratedOk" , 
        url :  'https://ruleschecker.abc-federation.gaia-x.community/rules/compliance/complianceParticipant' ,  
        testfile : './datas/rules-checker/provider-sample.json',
        testResult : function (body) {  return body.statut=="true" },
        type: "post"
    },
    {
        name:"testParticipantSignatureKo" , 
        url :  'https://ruleschecker.abc-federation.gaia-x.community/rules/compliance/complianceParticipant' ,  
        testfile : './datas/rules-checker/provider-proof-false.json',
        testResult : function (body) {  return body.statut=="false" },
        type: "post"
    },
    {
        name:"testSchemaValidatorKo" , 
        url :  'https://schemavalidator.abc-federation.gaia-x.community/provider' ,  
        testfile : './datas/schema-validator/test-ko.json',
        testResult : function (body) {  return !JSON.parse(body).statut },
        type: "post"
    },
    {
        name:"testLabellingRulesOk" , 
        url :  'https://ruleschecker.abc-federation.gaia-x.community/rules/labelling/label' ,
        testfile : './datas/rules-checker/labelling-ok.json',
        testResult : function (body) {  return body.statut=="true" },
        type: "post"
    },
    {   
        name:"testServiceOfferingRulesOk" , 
        url :  'https://ruleschecker.abc-federation.gaia-x.community/rules/compliance/complianceServiceOffering' ,  
        testfile : './datas/rules-checker/serviceOffering-ok.json',
        testResult : function (body) {  return body.statut=="true" },
        type: "post"
    },
    {   
        name:"testServiceOfferingRulesKo" , 
        url :  'https://ruleschecker.abc-federation.gaia-x.community/rules/compliance/complianceServiceOffering' ,  
        testfile : './datas/rules-checker/serviceOffering-ko.json',
        testResult : function (body) {  return body.statut=="false" },
        type: "post"
    },
    { 
        name:"testLabellingRulesKo" , 
        url :  'https://ruleschecker.abc-federation.gaia-x.community/rules/labelling/label' ,
        testfile : './datas/rules-checker/labelling-ko.json',
        testResult : function (body) {  return body.statut=="false" },
        type: "post"
    },
    {   
        name:"testLabellingRulesOkLabel1" , 
        url :  'https://ruleschecker.abc-federation.gaia-x.community/rules/labelling/label' ,
        testfile : './datas/rules-checker/labelling-ok-label1.json',
        testResult : function (body) {  return (body.statut=="true" && body.VC.credentialSubject.label == 1) },
        type: "post"
    }
];

export default checks;